/*
 * Author: Jack Booth
 * Date: 9/28/16
 * 
 * Generates a random pattern from two input characters
 */
import java.util.Scanner;

public class RandomPattern
{
	public static void main(String[] args)
	{
//		Takes user input as a string
		Scanner in = new Scanner(System.in);
		System.out.println("Enter two characters to generate a random pattern.");
		String input = in.nextLine();
		in.close();
		
//		seperates the two parts of the string into two characters
		char char1 = input.charAt(0);
		char char2 = input.charAt(1);
		
//		Generates random character with half chance of either of the two characters occurring, then prints characters in a 100x100 output.
		double random = Math.random();
		for(int i = 1; i <= 100; i++)
		{
			for(int j = 1; j<= 100; j++)
			{
				random = Math.random();
				if(random <.5)
				{
					System.out.print(char1);
				}
				else if(random>.5)
				{
					System.out.print(char2);
				}
			}
			random = Math.random();
			if(random <.5)
			{
				System.out.println(char1);
			}
			else if(random>.5)
			{
				System.out.println(char2);
			}
		}
	}
}
